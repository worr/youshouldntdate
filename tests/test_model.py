from datetime import datetime
import pytest
import os
from sqlalchemy.orm.exc import NoResultFound

os.environ['ENVIRONMENT'] = 'unittesting'
NOW = datetime.now()

import youshouldntdate.app  # noqa
from youshouldntdate import models  # noqa


@pytest.fixture(scope='module')
def db():
    n_rows = 5
    cur = 0
    up_cur = 0

    models.db.metadata.drop_all(bind=models.db.engine)
    models.db.session.commit()

    models.db.metadata.create_all(bind=models.db.engine)
    models.db.session.commit()

    class TestSubmission(models.Submission):
        def __init__(self):
            nonlocal cur

            kwargs = dict(
                id=str(cur),
                author_id='author_{}'.format(cur),
                author='author',
                created=NOW,
                title='title_{}'.format(cur),
                link='link_{}'.format(cur),
                subreddit='relationships',
                score=cur,
                last_update=NOW,
                text='text {}'.format(cur),
            )

            super().__init__(**kwargs)

            cur += 1

    class TestChildSubmission(models.Submission):
        def __init__(self):
            nonlocal up_cur

            kwargs = dict(
                id=str(up_cur + n_rows),
                author_id='author_{}'.format(up_cur),
                author='author',
                created=NOW,
                title='update: title_{}'.format(up_cur),
                link='link_{}'.format(up_cur),
                subreddit='relationships',
                score=up_cur,
                last_update=NOW,
                parent_id=str(up_cur),
                text='text {}'.format(cur),
            )

            super().__init__(**kwargs)

            up_cur += 1

    for _ in range(n_rows):
        models.db.session.add(TestSubmission())
        models.db.session.add(TestChildSubmission())

    models.db.session.commit()

    yield

    models.db.session.commit()

    models.db.metadata.drop_all(bind=models.db.engine)
    models.db.session.commit()


def test_get_submissionlist(db):
    sl = models.SubmissionList.from_id('1')

    assert len(sl) == 2
    assert sl[0].id == '1'
    assert sl[1].id == '6'


def test_delete_submissionlist(db):
    sl = models.SubmissionList.from_id('2')

    assert len(sl) == 2
    sl.delete()
    models.db.session.commit()

    with pytest.raises(NoResultFound):
        models.SubmissionList.from_id('2')

    with pytest.raises(NoResultFound):
        models.Submission.query.filter(models.Submission.id == '7').one()
