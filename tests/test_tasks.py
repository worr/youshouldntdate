#!/usr/bin/env python
# vim:set fileencoding=utf8:

"""
test_youshouldntdate
----------------------------------

Tests for `youshouldntdate` module.
"""

import mock  # type: ignore
import youshouldntdate.tasks
from youshouldntdate.tasks import (filter_removed, filter_broken_chains,
                                   filter_noysd, needs_deletion,
                                   update_post_list)


def test_filter_removed():
    class MockSub():
        def __init__(self, selftext):
            self.selftext = selftext

    test_good = (MockSub('test'), MockSub('bar'))
    assert filter_removed(test_good) is False

    bad_cases = (
        (MockSub('test'), MockSub('[removed]')),
        (MockSub('[removed]'), MockSub('test')),
        (MockSub('[deleted]'), MockSub('test')),
        (MockSub('test'), MockSub('[deleted]'))
    )

    for case in bad_cases:
        assert filter_removed(case) is True


def test_filter_broken_chains():
    class MockSub():
        def __init__(self, title):
            self.title = title

    good_cases = (
        (MockSub('test'), MockSub('[update] test')),
        (MockSub('test'), MockSub('[UPDATED] test')),
        (MockSub('test'), MockSub('UPDATE: test'))
    )

    for case in good_cases:
        assert filter_broken_chains(case) is False

    bad_cases = (
        (MockSub('[update] test'),),
        (MockSub('[UPDATED] test'),),
        (MockSub('UPDATE: test'),)
    )

    for case in bad_cases:
        assert filter_broken_chains(case) is True


def test_filter_noysd():
    class MockSub():
        def __init__(self, title):
            self.title = title

    good_cases = (
        (MockSub('test'), MockSub('[update] test')),
        (MockSub('test'), MockSub('[UPDATED] test')),
        (MockSub('test'), MockSub('UPDATE: test')),
        (MockSub('test'), MockSub('[removed]')),
        (MockSub('[removed]'), MockSub('test')),
        (MockSub('[deleted]'), MockSub('test')),
        (MockSub('test'), MockSub('[deleted]')),
        (MockSub('[update] test'),),
        (MockSub('[UPDATED] test'),),
        (MockSub('UPDATE: test'),)
    )

    for case in good_cases:
        assert filter_noysd(case) is False

    bad_cases = (
        (MockSub('[noysd] test'), MockSub('[update] test')),
        (MockSub('[NOYSD] test'), MockSub('[UPDATED] test')),
        (MockSub('test'), MockSub('[noysd] UPDATE: test')),
    )

    for case in bad_cases:
        assert filter_noysd(case) is True


def test_needs_deletion():
    class MockSub():
        def __init__(self, title, selftext=''):
            self.title = title
            self.selftext = selftext

    good_cases = (
        (MockSub('test'), MockSub('[update] test')),
        (MockSub('test'), MockSub('[UPDATED] test')),
        (MockSub('test'), MockSub('UPDATE: test')),
    )

    for case in good_cases:
        assert needs_deletion(case) is False

    bad_cases = (
        (MockSub('[noysd] test'), MockSub('[update] test')),
        (MockSub('[NOYSD] test'), MockSub('[UPDATED] test')),
        (MockSub('test'), MockSub('[noysd] UPDATE: test')),
        (MockSub('[update] test'),),
        (MockSub('[UPDATED] test'),),
        (MockSub('UPDATE: test'),),
        (MockSub('test'), MockSub('[update] test', selftext='[removed]')),
        (MockSub('test'), MockSub('[UPDATED] test', selftext='[deleted]')),
        (MockSub('test', selftext='[removed]'), MockSub('UPDATE: test')),
    )

    for case in bad_cases:
        assert needs_deletion(case) is True


def test_update_post_list():
    class PostMock(mock.Mock):
        @property
        def title(self):
            return ''

    def update_producer():
        ret = mock.Mock()
        ret.title = 'update: foo'
        return ret

    class PostListMock(list):
        delete_called = False

        def delete(self):
            self.delete_called = True

    youshouldntdate.tasks.praw = mock.Mock()
    youshouldntdate.tasks.RedditSubmission = mock.Mock()
    postlist = PostListMock((PostMock(), ))
    postlist.extend([update_producer() for _ in range(5)])
    update_post_list(postlist)
    for post in postlist:
        assert post.update_from_reddit_submission.called

    assert not postlist.delete_called

    postlist = PostListMock((update_producer() for _ in range(5)))
    update_post_list(postlist)
    for post in postlist:
        assert post.update_from_reddit_submission.called

    assert postlist.delete_called
