bumpversion==0.5.3
flake8==2.5.1
mypy-lang==0.2.0
nose==1.3.7
tox==2.1.1
