#!/usr/bin/env python
# vim:set fileencoding=utf8:


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


with open('README.mkd') as readme_file:
    readme = readme_file.read()

requirements = [
    "celery",
    "Flask",
    "Flask-Babel",
    "Flask-Bootstrap",
    "Flask-Markdown",
    "Flask-Migrate",
    "Flask-Nav",
    "Flask-Script",
    "Flask-SQLAlchemy",
    "orderedset",
    "praw",
    "psycopg2",
    "raven",
    "SQLAlchemy"
]

test_requirements = [
    "flake8",
    "mypy-lang",
    "nose",
]

setup(
    name='youshouldntdate',
    version='0.1.0',
    description="A website demonstrating the perils of dating in the modern era",
    long_description=readme,
    author="William Orr",
    author_email='will@worrbase.com',
    url='https://gitlab.com/worr/youshouldntdate',
    packages=[
        'youshouldntdate',
    ],
    package_dir={'youshouldntdate':
                 'youshouldntdate'},
    include_package_data=True,
    install_requires=requirements,
    license="MIT",
    scripts=['scripts/set-update-time'],
    zip_safe=False,
    keywords='youshouldntdate',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='nose.collector',
    tests_require=test_requirements
)
