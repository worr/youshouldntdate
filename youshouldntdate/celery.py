from datetime import timedelta
import flask  # type: ignore
from celery import Celery  # type: ignore
from raven import Client as RavenClient  # type: ignore
from raven.contrib.celery import register_signal, register_logger_signal  # type: ignore


# This is not how you make celery...
def make_celery(app: flask.Flask) -> Celery:
    CELERYBEAT_SCHEDULE = {
        'search-new-threads': {
            'task': 'youshouldntdate.tasks.searcher',
            'schedule': timedelta(minutes=30),
            'args': None
        },
        'update-old-threads': {
            'task': 'youshouldntdate.tasks.update',
            'schedule': timedelta(minutes=90),
            'args': None
        },
    }

    CELERY_ROUTES = {
        'youshouldntdate.tasks.searcher': {
            'queue': 'searcher'
        },
        'youshouldntdate.tasks.store': {
            'queue': 'searcher'
        },
        'youshouldntdate.tasks.update': {
            'queue': 'update'
        },
        'youshouldntdate.tasks.update_post_list': {
            'queue': 'update'
        },
    }

    app.config['CELERYBEAT_SCHEDULE'] = CELERYBEAT_SCHEDULE
    app.config['CELERY_TASK_SERIALIZER'] = 'pickle'
    app.config['CELERY_ACCEPT_CONTENT'] = ('pickle',)
    app.config['CELERY_TIMEZONE'] = 'UTC'
    app.config['CELERY_ROUTES'] = CELERY_ROUTES

    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):  # type: ignore
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask

    # Feed badness into Sentry
    if app.config['SENTRY_ENABLED']:
        raven_client = RavenClient(app.config['SENTRY_DSN'])
        register_logger_signal(raven_client)
        register_signal(raven_client)

    return celery
