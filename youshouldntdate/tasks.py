from functools import lru_cache, reduce
import re
from orderedset import OrderedSet  # type: ignore
import praw  # type: ignore
from praw.objects import Submission as RedditSubmission  # type: ignore
import sqlalchemy.exc  # type: ignore
from typing import Callable, List, Optional, Sequence, Tuple  # noqa

from .app import app, celery, db
from .exceptions import OPNotFoundError
from .models import Submission, SubmissionList


@celery.task
def searcher() -> None:
    reddit = praw.Reddit(user_agent=app.config['REDDIT_USERAGENT'])
    for subreddit in app.config['REDDIT_SUBREDDITS']:
        print(subreddit)
        posts = reddit.get_subreddit(subreddit).search('title:UPDATE self:yes', sort='new', limit=100)
        for post in posts:
            if Submission.query.filter_by(id=post.id).count():
                continue
            store.delay(post, subreddit)


short_re = re.compile(r'https?://redd\.it/(\w+)')
long_re = re.compile(r'https?://\w+\.reddit\.com/r/\w+/comments/(\w+)/')


@lru_cache()
def get_id(id_: str, text: str) -> OrderedSet:
    matches = []
    for regex in (short_re, long_re):
        matches.extend([item for item in regex.finditer(text)])
    if not matches:
        raise OPNotFoundError(id_)
    return OrderedSet([match.group(1) for match in matches])


@lru_cache()
def get_submission(id_: str, reddit: praw.Reddit) -> RedditSubmission:
    return reddit.get_submission(submission_id=id_)


def get_parents(post: RedditSubmission, wset: OrderedSet=None, count: int=0) -> OrderedSet:
    if wset is None:
        wset = OrderedSet()

    if count >= 9:
        return wset

    try:
        parent_ids = get_id(post.id, post.selftext)
    except OPNotFoundError:
        wset.add(post)
        return wset

    reddit = praw.Reddit(user_agent=app.config['REDDIT_USERAGENT'])
    for idx, parent_id in enumerate(parent_ids):
        if idx == 0:
            wset = get_parents(get_submission(parent_id, reddit), wset, count + 1)
        else:
            wset.add(get_submission(parent_id, reddit))
    wset.add(post)
    return wset


# true if we need to remove this set of posts
SubmissionFilter = Callable[[Sequence[RedditSubmission]], bool]


def filter_removed(posts: Sequence[RedditSubmission]) -> bool:
    for post in posts:
        if post.selftext in ('[deleted]', '[removed]'):
            return True
    return False


def filter_broken_chains(posts: Sequence[RedditSubmission]) -> bool:
    return 'update' in posts[0].title.lower()


def filter_noysd(posts: Sequence[RedditSubmission]) -> bool:
    return reduce(lambda cum, post: cum or '[noysd]' in post.title.lower(), posts, False)


post_filters = (filter_removed, filter_broken_chains, filter_noysd)  # type: Tuple[SubmissionFilter, ...]


def needs_deletion(posts: Sequence[RedditSubmission]) -> bool:
    def run_filter(cum: bool, filt: SubmissionFilter):
        if cum:
            return cum
        return filt(posts)

    return reduce(run_filter, post_filters, False)


@celery.task
def store(post: RedditSubmission, subreddit: str) -> None:
    reddit = praw.Reddit(user_agent=app.config['REDDIT_USERAGENT'])
    post = reddit.get_info(thing_id=post.fullname)
    parents = sorted(get_parents(post), key=lambda x: x.created_utc)
    if needs_deletion(parents):
        print("ditching post {}".format(parents[0].id))
        return

    last_update = parents[-1].created_utc
    for idx, parent in enumerate(parents):
        if idx != 0:
            db.session.add(
                Submission.from_reddit_submission(
                    parent,
                    subreddit,
                    parents[idx - 1].id,
                    last_update=last_update
                )
            )
        else:
            db.session.add(
                Submission.from_reddit_submission(
                    parent,
                    subreddit,
                    last_update=last_update
                )
            )

        try:
            db.session.commit()
        except sqlalchemy.exc.IntegrityError as e:
            print(e)
            db.session.rollback()


@celery.task
def update_post_list(postlist: SubmissionList) -> None:
    reddit = praw.Reddit(user_agent=app.config['REDDIT_USERAGENT'])
    for post in postlist:
        print("Updating post {}".format(post.id))
        reddit_post = RedditSubmission.from_url(reddit, post.link)
        post.update_from_reddit_submission(reddit_post)
    if needs_deletion(postlist):
        postlist.delete()


@celery.task
def update(posts: Optional[List[SubmissionList]]=None) -> None:
    if not posts:
        posts = []  # List[SubmissionList]
        parents = Submission.query.filter(
            Submission.archived == False and \
            Submission.parent_id == None  # noqa
        )

        for parent in parents:
            posts.append(SubmissionList.from_id(parent.id))

    for postlist in posts:
        update_post_list.delay(postlist)
