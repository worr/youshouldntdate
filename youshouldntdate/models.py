from datetime import datetime
from praw.objects import Submission as RedditSubmission, Redditor  # type: ignore
from praw.errors import PRAWException  # type: ignore
from flask_sqlalchemy import Model  # type: ignore # noqa
from sqlalchemy.sql import func  # type: ignore
from typing import Any, List, Optional

from .app import db

DbModel = db.Model  # type: Model


class Submission(DbModel):
    __tablename__ = 'submissions'

    # use reddit's id
    id = db.Column(db.String, primary_key=True)
    author_id = db.Column(db.String)
    author = db.Column(db.String)
    created = db.Column(db.DateTime(True), nullable=False)
    edited = db.Column(db.DateTime(True))
    text = db.Column(db.String, nullable=False)
    title = db.Column(db.String, nullable=False)
    parent_id = db.Column(db.String, db.ForeignKey('submissions.id'))
    update = db.relationship('Submission', uselist=False)
    link = db.Column(db.String, nullable=False)
    hidden = db.Column(db.Boolean, nullable=False, server_default='False')
    subreddit = db.Column(db.String, nullable=False)
    score = db.Column(db.Integer, nullable=False)
    archived = db.Column(db.Boolean, nullable=False, server_default='False')
    last_update = db.Column(db.DateTime(True), nullable=False, server_default=func.now())

    @property
    def selftext(self) -> str:
        return self.text

    @property
    def np_link(self) -> str:
        return self.link.replace('www', 'np')

    @staticmethod
    def get_author(author: Redditor) -> Any:
        class EmptyAuthor():
            @property
            def id(self):
                return None

            @property
            def name(self):
                return None

        if not author:
            return EmptyAuthor()

        try:
            author.id
            return author
        except (PRAWException, AttributeError):
            return EmptyAuthor()

    @classmethod
    def from_reddit_submission(cls: type, post: RedditSubmission, subreddit: str, parent_id: Optional[str]=None, last_update: Optional[float]=None) -> Any:
        author = Submission.get_author(post.author)
        s = cls(
            id=post.id,
            author=author.name,
            author_id=author.id,
            created=datetime.fromtimestamp(post.created_utc),
            edited=datetime.fromtimestamp(post.edited) if post.edited else None,
            text=post.selftext,
            title=post.title,
            link=post.permalink,
            subreddit=subreddit,
            score=post.score,
            archived=post.locked,
            parent_id=parent_id,
            last_update=datetime.fromtimestamp(last_update))
        return s

    def update_from_reddit_submission(self, post: RedditSubmission) -> None:
        author = Submission.get_author(post.author)
        self.author = author.name
        self.author_id = author.id
        self.edited = datetime.fromtimestamp(post.edited) if post.edited else None
        self.text = post.selftext
        self.score = post.score
        self.archived = post.locked
        db.session.commit()


class SubmissionList(list):
    @classmethod
    def from_id(cls: type, id_: str) -> Any:
        ret = []  # type: List[Submission]
        top = Submission.query.filter_by(id=id_).one()
        ret.extend(cls.get_children(top))
        return cls(ret)

    @staticmethod
    def get_children(sub: Submission, wset: Optional[List[Submission]]=None) -> List[Submission]:
        if wset is None:
            wset = []
        wset.append(sub)
        posts = Submission.query.filter_by(parent_id=sub.id).all()
        for post in posts:
            wset = SubmissionList.get_children(post, wset)
        return wset

    def delete(self) -> None:
        for post in self[::-1]:
            db.session.delete(post)
        db.session.commit()


class User(DbModel):
    id = db.Column(db.String, primary_key=True)
    user = db.Column(db.String)
