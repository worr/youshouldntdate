class YouShouldntDateError(Exception):
    pass


class OPNotFoundError(YouShouldntDateError):
    def __init__(self, id_):
        self.id = id_

    def __str__(self):
        return "Could not find link in {}".format(self.id)
