from . import __version__, __name__
import os


class Config():
    CELERY_IGNORE_RESULT = True
    DEBUG = False
    REDDIT_CLIENT_ID = os.getenv('REDDIT_CLIENT_ID')
    REDDIT_CLIENT_SECRET = os.getenv('REDDIT_CLIENT_SECRET')
    REDDIT_SUBREDDITS = ['relationships', 'relationship_advice']
    REDDIT_USERAGENT = 'web:{}:{} (by /u/worr)'.format(
        __name__, __version__)
    SECRET_KEY = None  # type: bytes
    SENTRY_ENABLED = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    TESTING = False


class TestingConfig(Config):
    CELERY_BROKER_URL = 'amqp://localhost'
    CELERY_RESULT_BACKEND = 'amqp://localhost'
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://ysd:ysd@localhost:5432/youshouldntdate'
    SECRET_KEY = os.urandom(24)
    DEBUG = True
    TESTING = True
    REDDIT_REDIRECT = 'http://10.0.0.34:5000/authorize'


class UnitTestConfig(TestingConfig):
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://ysd:ysd@localhost:5432/youshouldntdate_test'


class ProductionConfig(Config):
    CELERY_BROKER_URL = os.getenv('CLOUDAMQP_URL')
    CELERY_RESULT_BACKEND = os.getenv('CLOUDAMQP_URL')
    SECRET_KEY = os.getenv('SECRET_KEY')
    REDDIT_REDIRECT = 'http://youshouldntdate.heroku.com/authorize'
    SENTRY_ENABLED = True
    SENTRY_DSN = os.getenv('SENTRY_DSN')
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')
