from base64 import b64encode
from flask_sqlalchemy import Model  # type: ignore # noqa
from flask import Flask, redirect, render_template, Response  # type: ignore
from flask import request, session, url_for  # type: ignore
from flask_babel import Babel  # type: ignore
from flask_sqlalchemy import SQLAlchemy  # type: ignore
from flaskext.markdown import Markdown  # type: ignore
from flask_bootstrap import Bootstrap  # type: ignore
from flask_nav import Nav  # type: ignore
from flask_nav.elements import Navbar, View  # type: ignore
from functools import wraps
import os
from praw import Reddit  # type: ignore
from raven.contrib.flask import Sentry  # type: ignore
from typing import Callable

from .celery import make_celery


_APP = None
_DB = None
_REDDIT = None


def make_app() -> Flask:
    global _APP
    if _APP is None:
        app = Flask(__name__)
        if os.getenv('ENVIRONMENT') == 'production':
            app.config.from_object('youshouldntdate.config.ProductionConfig')
        elif os.getenv('ENVIRONMENT') == 'unittesting':
            app.config.from_object('youshouldntdate.config.UnitTestConfig')
        else:
            app.config.from_object('youshouldntdate.config.TestingConfig')

        Markdown(app)
        Bootstrap(app)
        Babel(app)

        nav = Nav()
        nav.register_element('top', Navbar(View('youshouldnt.date', 'index')))
        nav.init_app(app)

        if app.config['SENTRY_ENABLED']:
            Sentry().init_app(app, dsn=app.config['SENTRY_DSN'], logging=True)
        _APP = app
    return _APP


def make_db(app: Flask) -> SQLAlchemy:
    global _DB
    if _DB is None:
        db = SQLAlchemy(app)
        _DB = db
    return _DB


def make_reddit(app: Flask) -> Reddit:
    global _REDDIT
    if _REDDIT is None:
        r = Reddit(user_agent=app.config['REDDIT_USERAGENT'])
        r.set_oauth_app_info(
            client_id=app.config['REDDIT_CLIENT_ID'],
            client_secret=app.config['REDDIT_CLIENT_SECRET'],
            redirect_uri=app.config['REDDIT_REDIRECT'])
        _REDDIT = r
    return _REDDIT


app = make_app()
db = make_db(app)  # type: SQLAlchemy
celery = make_celery(app)
reddit = make_reddit(app)

from .models import Submission, SubmissionList  # noqa


def loggedin(fn: Callable[..., Response]):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        try:
            reddit.set_access_credentials('identity', session['access_token'], session['refresh_token'])
        except KeyError:
            reddit.clear_authentication()
        return fn(*args, **kwargs)
    return wrapper


@app.route("/")
@loggedin
def index() -> Response:
    page = request.args.get('page', None)

    if page is not None:
        try:
            page = int(page)
        except ValueError:
            page = None

    subs = Submission.query.filter(Submission.parent_id == None, Submission.hidden == False).order_by(  # noqa
        Submission.last_update.desc()).paginate(page=page, per_page=20)
    return render_template("index.html", posts=subs)


@app.route("/post/<id_>", methods=["GET"])
@loggedin
def redditpost(id_: str) -> Response:
    return render_template("posts.html", posts=SubmissionList.from_id(id_))


@app.route("/login", methods=["GET"])
def login() -> Response:
    unique = b64encode(os.urandom(128))
    session['unique'] = unique
    url = reddit.get_authorize_url(unique, 'identity', True)

    return redirect(url)


@app.route("/authorize", methods=["GET"])
def authorize() -> Response:
    session['code'] = request.args.get('code')

    access_info = reddit.get_access_information(session['code'])
    session['access_token'] = access_info['access_token']
    session['refresh_token'] = access_info['refresh_token']
    return redirect(url_for('index'))
